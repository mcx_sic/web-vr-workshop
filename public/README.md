# WebVR Boilerplate

A [THREE.js][three]-based starting point for VR experiences that work well in
both Google Cardboard and other VR headsets. Also provides a fallback for
experiencing the same content without requiring a VR device.

This project relies heavily on the [webvr-polyfill][polyfill] to provide VR
support if the [WebVR API](spec) is not implemented.

[three]: http://threejs.org/
[polyfill]: https://github.com/borismus/webvr-polyfill


## Features

As of WebVR 1.0, this project relies on the polyfill for even more. Core
features like [lens distortion][distortion] and device detection have moved into the polyfill.
This project now acts as a getting started example, and provides a reasonable
user experience for getting in and out of Virtual Reality and Magic Window
modes.

As a convenience, the WebVRManager emits certain `modechange` events, which can
be subscribed using `manager.on('modechange', callback)`.


## Getting started

The easiest way to start is to fork this repository or copy its contents into a
new directory.

The boilerplate is also available via npm. Easy install:

    npm install webvr-boilerplate


## Thanks

- [Brandon Jones][bj] and [Vladimir Vukicevic][vv] for their work on the [WebVR
  spec][spec].
- [Ricardo Cabello][doob] for THREE.js.
- [Diego Marcos][dm] for VREffect and VRControls.
- [Dmitry Kovalev][dk] for help with [lens distortion correction][distortion].

[dk]: https://plus.google.com/+DmitryKovalev1
[distortion]: https://github.com/borismus/webvr-polyfill/blob/master/src/distortion/distortion.js
[bj]: https://twitter.com/tojiro
[vv]: https://twitter.com/vvuk
[spec]: https://mozvr.github.io/webvr-spec/
[dm]: https://twitter.com/dmarcos
[doob]: https://twitter.com/mrdoob
