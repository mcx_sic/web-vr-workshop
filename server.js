/**
 * Created by michelcomin on 9/6/16.
 */


// server.js
var express = require('express');
var app = express();
var httpServer = require("http").createServer(app);
var five = require("johnny-five");
var io = require('socket.io')(httpServer);

var port = 3000;

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

httpServer.listen(port);
console.log('Server available at http://localhost:' + port);
var led;

//Arduino board connection

var board = new five.Board();
board.on("ready", function () {
    console.log('Arduino connected');
    led = new five.Led(2);
});

//Socket connection handler
io.on('connection', function (socket) {
    console.log(socket.id);

    socket.on('led:on', function (ledOn) {
        if (typeof led === "undefined")
            return;

        ledOn ? led.on() : led.off();
        console.log('LED ON RECEIVED');
        socket.emit('is:led:on', ledOn ? "LED IS ON" : "NO LED ON");
    });

    //socket.on('key:down', function (key) {
    //    if (typeof led === "undefined")
    //        return;
    //
    //    if (key == "90") {
    //        led.on();
    //        socket.emit("key:received", "key has been received");
    //    } else {
    //        led.off();
    //    }
    //
    //    console.log('key: ' + key);
    //});
});

console.log('Waiting for connection');


