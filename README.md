# Web VR Workshop

A [THREE.js][three]-based starting point for VR experiences that work well in
both Google Cardboard and other VR headsets. Also provides a fallback for
experiencing the same content without requiring a VR device.

This project relies is based on the [WebVR Boilerplate][boilerplate].

[three]: http://threejs.org/
[boilerplate]: https://github.com/borismus/webvr-boilerplate

## Features

As of WebVR 1.0, this project relies on the polyfill for even more. Core
features like [lens distortion][distortion] and device detection have moved into the polyfill.
This project now acts as a getting started example, and provides a reasonable
user experience for getting in and out of Virtual Reality and Magic Window
modes.

As a convenience, the WebVRManager emits certain `modechange` events, which can
be subscribed using `manager.on('modechange', callback)`.

### Requirements

1. [Node.js](http://nodejs.org/download/) (required)
1. [Arduino Software](https://www.arduino.cc/en/Main/Software) (required)

## Workshop steps

### Prepare the Arduino
1. Open the Arduino App with the Arduino board connected to the Computer.
2. Navigate to `File > Examples > Firmdata > StandardFirmdata`.
3. Navigate to `Tools > Board` and select yours.
4. Navigate to `Tools > Port` and select the one where Arduino is connected.
5. Click the upload button.

## Getting started
1. Fetch dependencies:

	```sh
	$ npm install
	```
4. Start the server:

	```sh
	$ npm start
	```
3. Open a browser and navigate to [http://localhost:3000/]()
4. you can edit `server.js` (remember to stop and do `npm start` when you modify the server) and `public/js/main.js`;


## Tech

kind|name|url
---|---|---
server|**Node.js**|[http://nodejs.org/]()
server/arduino|**Johnny Five**|[https://github.com/rwaldron/johnny-five]()
communication|**socket.io**|[http://socket.io/]()
3D Graphics|**three.js**|[http://threejs.org/]()

